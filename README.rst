OpenStack-Ansible py_from_git role
##################################

This Ansible role is no longer maintained or used.

For any further questions, please email
openstack-dev@lists.openstack.org or join #openstack-ansible on
Freenode.
